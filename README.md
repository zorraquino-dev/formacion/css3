# Contenido formación CSS3

* CSS3
  * Selectores: Básicos, Avanzados, Pseudo-clases y Pseudo-elementos.
  * Especificidad.
  * Unidades de medida: Absolutas, Relativas y función calc().
  * Tipografía: @font-face, FOIT vs FOUT, Google Fonts y rendimiento.
  * Transiciones.
  * Animaciones (@keyframes).
  * Maquetación Clásica Flotante.
  * Variables.
  * Maquetación Flexbox.
  * Maquetación CSS Grid.


* CSS3 y HTML
  * Estructura: header, footer, main, section, article y aside.
  * Imágenes y Picture.
  * SVG: Uso y manipulación de iconografías.
  * Vídeo y Audio.
  * Listas.
  * Tablas.
  * Formularios.
  * Atributos data.
  * Etiquetas Meta.


* Preprocesadores
  * SASS: Variables, Anidamientos, Mixins.
  * Otros pre-procesadores.


* CSS3 y JavaScript
  * Introducción a Javascript Nativo.
  * Creando y manipulando estilos mediante JavaScript.



## CSS3

### Selectores Básicos

Más información en [MDN Web Docs](https://developer.mozilla.org/es/docs/Web/CSS/Selectores_CSS#Selectores_b%C3%A1sicos)

### Selectoreas Avanzados

Más información en [MDN Web Docs](https://developer.mozilla.org/es/docs/Web/CSS/Selectores_CSS#Combinadores)

### Pseudo-clases

**Tipos de pseudo-clases:**

|                  |                       |                 |                 |
| ---              | ---                   | ---             | ---             |
| `:active` | `:fullscreen` | `:link` | `:read-only` |
| `:checked` | `:focus` | `:not()` | `:read-write` |
| `:default` | `:hover` | `:nth-child()` | `:required` |
| `:dir()` | `:indeterminate` | `:nth-last-child()` | `:right` |
| `:disabled` | `:in-range` | `:nth-last-of-type()` | `:root` |
| `:empty` | `:invalid` | `:nth-of-type()` | `:scope` |
| `:enabled` | `:lang()` | `:only-child` | `:target` |
| `:first` | `:last-child` | `:only-of-type` | `:valid` |
| `:first-child` | `:last-of-type` | `:optional` | `:visited` |
| `:first-of-type` | `:left` | `:out-of-range` |

Más información en [MDN Web Docs](https://developer.mozilla.org/es/docs/Web/CSS/Pseudo-classes)


### Pseudo-elementos

**Tipos de pseudo-elementos:**

|                  |                 |                    |                    |
| ---              | ---             | ---                | ---                |
| `::after`        | `::first-line`  | `::placeholder`    | `::grammar-error`  |
| `::before`       | `::selection`   | `::marker`         |                    |
| `::first-letter` | `::backdrop`    | `::spelling-error` |                    |

Más información en [MDN Web Docs](https://developer.mozilla.org/es/docs/Web/CSS/Pseudoelementos)



### Especificidad

Más información en [MDN Web Docs](https://developer.mozilla.org/es/docs/Web/CSS/Especificidad)


### Unidades de medida

**Unidades absolutas**

|      |                             |
| ---  | ---                         |
| `in` | Pulgadas (1in = 25.4mm)     |
| `cm` | Centímetros (1cm = 10mm)    |
| `pc` | Picas (1pc = 4.23mm)        |
| `mm` | Milímetros (1mm = 1mm)      |
| `pt` | Puntos (1pt = 0.35mm)       |
| `px` | Píxels (1px = 0.26mm)       |
| `Q`  | Cuarto de mm (1Q = 0.248mm) |


**Unidades relativas**

|        |                                                                             |
| ---    | ---                                                                         |
| `%`    | Porcentaje relativo                                                         |
| `em`   | Tamaño de fuente del elemento (1em)                                         |
| `rem`  | Font size of the root element (1rem)                                        |
| `vw`   | 1% del ancho de la ventana (1vw)                                            |
| `vh`   | 1% del alto de la ventana (1vh)                                             |
| `vmin` | 1% de la dimensión menor de la ventana (1vmin)                              |
| `vmax` | 1% de la dimensión mayor de la ventana (1vmax)                              |
| `ex`   | Altura de la letra “x” de la fuente del elemento (1ex)                      |
| `ch`   | Avance promedio de caracteres del glifo “0” de la fuente del elemento (1ch) |
| `vi`   | 1% del tamaño de la ventana en la dirección de la línea (1vi)               |
| `vb`   | 1% del tamaño de la ventana en la dirección del bloque (1vb)                |

Más información en [Units you've never heard about](https://dev.to/maxart2501/the-new-and-old-css-units-youve-never-heard-about-1mn1)


### Tipografía



















